package com.yudenko.mdd;

import java.util.Random;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );

        Random random = new Random(441287210);
        for(int i=0;i<10;i++)
            System.out.print(random.nextInt(10)+" ");

        random = new Random(-6732303926L);
        for(int i=0;i<10;i++)
            System.out.println(random.nextInt(10)+" ");
    }
}
