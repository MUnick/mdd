package com.yudenko.mdd.timeouts;

import com.yudenko.mdd.domain.Payment;
import com.yudenko.mdd.exception.TimeoutException;

import java.util.List;

public class LongDurationOperation {
    private static final long TIME_OUT = 1000;
    private static final long TIME_OUT_PER_PAYMENT = 110;
    private List<Payment> payments;
    private volatile boolean isReady;
    private double result;

    public double averageCalc(List<Payment> payments) {
        this.payments = payments;
        isReady = false;
        long deadPoint = System.currentTimeMillis() + TIME_OUT;
        ThreadCalc threadCalc = new ThreadCalc();
        new Thread(threadCalc).start();
        while (!isReady && !isTimeOuted(deadPoint)) {
        }
        if (isReady) {
            return result;
        } else {
            throw new TimeoutException();
        }
    }

    public double averageCalcDynamic(List<Payment> payments) {
        this.payments = payments;
        isReady = false;
        long deadPoint = System.currentTimeMillis() + payments.size() * TIME_OUT_PER_PAYMENT;
        ThreadCalc threadCalc = new ThreadCalc();
        new Thread(threadCalc).start();
        while (!isReady && !isTimeOuted(deadPoint)) {
        }
        if (isReady) {
            return result;
        } else {
            throw new TimeoutException();
        }
    }

    private boolean isTimeOuted(long checkPoint) {
        return System.currentTimeMillis() > checkPoint;
    }

    private class ThreadCalc implements Runnable {

        @Override
        public void run() {
            try {
                Thread.sleep(100 * payments.size());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            result = payments.stream().mapToDouble(Payment::getAmount).average().orElse(0.0);
            isReady = true;
        }
    }
}
