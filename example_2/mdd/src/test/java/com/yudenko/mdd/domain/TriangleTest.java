package com.yudenko.mdd.domain;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;


class TriangleTest {

    @Test
    void shouldCreateObjectTriangle() {
        //GIVEN
        double a = 3, b = 4, c = 5;
        //THEN
        assertDoesNotThrow(() -> new Triangle(a, b, c));
    }

    @Test
    void shouldHaveException() {
        //GIVEN
        double a = 3, b = 4, c = 10;
        //WHEN
        Exception thrown = assertThrows(Exception.class, () -> new Triangle(a, b, c));
        //THEN
        Assertions.assertEquals("wrong parameters", thrown.getMessage());
    }

    @Test
    void shouldHaveExceptionWithOneNegativeParameters() {
        //GIVEN
        double a = -3, b = 4, c = 5;
        //WHEN
        Exception thrown = assertThrows(Exception.class, () -> new Triangle(a, b, c));
        //THEN
        Assertions.assertEquals("wrong parameters", thrown.getMessage());
    }

    @Test
    void shouldHaveExceptionWithTwoNegativeParameters() {
        //GIVEN
        double a = -3, b = -4, c = 5;
        //WHEN
        Exception thrown = assertThrows(Exception.class, () -> new Triangle(a, b, c));
        //THEN
        Assertions.assertEquals("wrong parameters", thrown.getMessage());
    }

    @Test
    void shouldHaveExceptionWithAllNegativeParameters() {
        //GIVEN
        double a = -3, b = -4, c = -5;
        //WHEN
        Exception thrown = assertThrows(Exception.class, () -> new Triangle(a, b, c));
        //THEN
        Assertions.assertEquals("wrong parameters", thrown.getMessage());
    }
}