package com.yudenko.mdd.fail;

import com.yudenko.mdd.domain.Payment;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;

class PaymentProcessorTest {

    private PaymentProcessor paymentProcessor;

    @BeforeEach
    void setUp() {
        paymentProcessor = new PaymentProcessor();
    }

    @Test
    void simplePayment() {
        // GIVEN
        Payment payment = new Payment(100.00, new Date(), 10.00);
        // WHEN
        boolean result = paymentProcessor.simplePayment(payment);
        // THEN
        assertTrue(result);
    }
}