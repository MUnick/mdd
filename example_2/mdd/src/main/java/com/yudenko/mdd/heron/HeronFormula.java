package com.yudenko.mdd.heron;

import com.yudenko.mdd.domain.Triangle;

import java.util.Optional;

public class HeronFormula {

    public static double areaTriangleOptimistic(Triangle triangle) {
        double a = triangle.getA();
        double b = triangle.getB();
        double c = triangle.getC();

        double halfPerimeter = (a + b + c) / 2;
        double arg = halfPerimeter
                * (halfPerimeter - a)
                * (halfPerimeter - b)
                * (halfPerimeter - c);
        return Math.sqrt(arg);
    }

    public static double areaTrianglePesimistic_Negative(Triangle triangle) {
        if (triangle == null) {
            throw new IllegalArgumentException("Input must not be null");
        }

        double a = triangle.getA();
        double b = triangle.getB();
        double c = triangle.getC();

        double halfPerimeter = (a + b + c) / 2;
        double arg = halfPerimeter
                * (halfPerimeter - a)
                * (halfPerimeter - b)
                * (halfPerimeter - c);
        return Math.sqrt(arg);
    }

    public static double areaTrianglePesimistic_Positive(Triangle triangle) {
        if (triangle != null) {
            double a = triangle.getA();
            double b = triangle.getB();
            double c = triangle.getC();

            double halfPerimeter = (a + b + c) / 2;
            double arg = halfPerimeter
                    * (halfPerimeter - a)
                    * (halfPerimeter - b)
                    * (halfPerimeter - c);
            return Math.sqrt(arg);
        }
        throw new IllegalArgumentException("Input must not be null");
    }

    public static double areaTriangleParanoic(Triangle triangle) throws Exception {
        if (isParametersValid(triangle.getA(), triangle.getB(), triangle.getC())) {
            double a = triangle.getA();
            double b = triangle.getB();
            double c = triangle.getC();

            double halfPerimeter = (a + b + c) / 2;
            double arg = halfPerimeter
                    * (halfPerimeter - a)
                    * (halfPerimeter - b)
                    * (halfPerimeter - c);
            return Math.sqrt(arg);
        } else {
            throw new Exception("wrong parameters");
        }
    }

    public static double areaTriangle(double a, double b, double c) throws Exception {
        if (isParametersValid(a, b, c)) {
            double halfPerimeter = (a + b + c) / 2;
            double arg = halfPerimeter
                    * (halfPerimeter - a)
                    * (halfPerimeter - b)
                    * (halfPerimeter - c);
            return Math.sqrt(arg);
        }
        throw new Exception("wrong parameters");
    }

    public static double areaTriangleOptimistic(double a, double b, double c) {
        double halfPerimeter = (a + b + c) / 2;
        double arg = halfPerimeter
                * (halfPerimeter - a)
                * (halfPerimeter - b)
                * (halfPerimeter - c);
        return Math.sqrt(arg);
    }

    private static boolean isParametersValid(double a, double b, double c) {
        return (b + c) > a && (a + c) > b && (a + b) > c;
    }
}
