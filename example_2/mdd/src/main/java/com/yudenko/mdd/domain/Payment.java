package com.yudenko.mdd.domain;

import java.util.Date;

public class Payment {

    private Double amount;
    private Date date;
    private Double tax;

    public Payment(Double amount, Date date, Double tax) {
        this.amount = amount;
        this.date = date;
        this.tax = tax;
    }

    public Double getAmount() {
        return amount;
    }

    public Date getDate() {
        return date;
    }

    public Double getTax() {
        return tax;
    }
}
