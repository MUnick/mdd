package com.yudenko.mdd.multithreading;

import com.yudenko.mdd.domain.Payment;
import org.junit.jupiter.api.Test;

import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class MultiThreadingTest {

    @Test
    void onPayment() {
        // GIVEN
        MultiThreading service = new MultiThreading();
        Date dateBefore = new Date();
        // WHEN
        service.onPayment(13,0.5);
        // THEN
        Date dateAfter = new Date();
        List<Payment> payments = service.getPayments();
        assertNotNull(payments);
        assertTrue(payments.size() == 1);
        assertEquals(13, payments.get(0).getAmount());
        assertEquals(0.5, payments.get(0).getTax());
        assertNotNull(payments.get(0).getDate()); // Is it enough? Has date been set correctly
        // check date is created correctly in range
        assertTrue(dateBefore.getTime() <= payments.get(0).getDate().getTime());
        assertTrue(payments.get(0).getDate().getTime() <= dateAfter.getTime());
    }

}