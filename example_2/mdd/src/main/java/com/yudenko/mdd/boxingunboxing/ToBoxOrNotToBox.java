package com.yudenko.mdd.boxingunboxing;

import com.yudenko.mdd.domain.Payment;

import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class ToBoxOrNotToBox {

    public double average(List<Payment> payments, Boolean isPastOnly) {
        return payments.stream()
                .filter(payment -> isSuitable(payment, isPastOnly))
                .mapToDouble(Payment::getAmount)
                .average()
                .orElse(0.00);
    }

    public List<Payment> filterByAmount(List<Payment> payments, Double min, Double max) {
        return payments.stream()
                .filter(payment -> payment.getAmount() > min)
                .filter(payment -> payment.getAmount() < max)
                .collect(Collectors.toList());
    }

    public double findMaxForLatest(List<Payment> payments, Integer numberPayments) {
        return payments.stream()
                .sorted(Comparator.comparing(Payment::getDate).reversed())
                .limit(numberPayments)
                .mapToDouble(Payment::getAmount)
                .max()
                .orElse(0.00);
    }

    private boolean isSuitable(Payment payment, boolean checkPast) {
        return !checkPast || payment.getDate().before(new Date());
    }
}
