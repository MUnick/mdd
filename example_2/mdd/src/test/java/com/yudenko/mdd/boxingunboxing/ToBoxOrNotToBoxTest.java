package com.yudenko.mdd.boxingunboxing;

import com.yudenko.mdd.domain.Payment;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class ToBoxOrNotToBoxTest {

    @Test
    void average() {
        // GIVEN
        ToBoxOrNotToBox service = new ToBoxOrNotToBox();
        // WHEN
        double avrg = service.average(preparePayments(), false);
        // THEN
        assertEquals(57.5, avrg, 0.0001);
    }

    @ParameterizedTest
    @MethodSource("pastConfig")
    void averagePessimistic(Boolean isPast) {
        // GIVEN
        ToBoxOrNotToBox service = new ToBoxOrNotToBox();
        // WHEN
        double avrg = service.average(preparePayments(), isPast);
        // THEN
        assertTrue(avrg > 0);
    }

    @ParameterizedTest
    @MethodSource("filterConfig")
    void filterPayments(Double min, Double max) {
        // GIVEN
        ToBoxOrNotToBox service = new ToBoxOrNotToBox();
        // WHEN
        List<Payment> filteredPayments = service.filterByAmount(preparePayments(), min, max);
        // THEN
        assertEquals(1, filteredPayments.size());
    }

    @ParameterizedTest
    @MethodSource("maxConfig")
    void findMaxForLatest(Integer maxNumberPayment, double expected) {
        // GIVEN
        ToBoxOrNotToBox service = new ToBoxOrNotToBox();
        // WHEN
        double max = service.findMaxForLatest(preparePayments(), maxNumberPayment);
        // THEN
        assertEquals(expected, max);
    }

    private List<Payment> preparePayments() {
        List<Payment> payments = new ArrayList<>();
        payments.add(new Payment(15.00, new Date(new Date().getTime() + 1000), 10.00));
        payments.add(new Payment(100.00, new Date(new Date().getTime() - 1000), 1.00));
        return payments;
    }

    private static Stream<Arguments> maxConfig() {
        Integer empty = null;
        return Stream.of(
                Arguments.of(empty, 0.00),
                Arguments.of(0, 0.00),
                Arguments.of(1, 15.00),
                Arguments.of(2, 100.00),
                Arguments.of(100, 100.00)
        );
    }

    private static Stream<Arguments> pastConfig() {
        Boolean empty = null;
        return Stream.of(
                Arguments.of(empty),
                Arguments.of(Boolean.TRUE),
                Arguments.of(Boolean.FALSE),
                Arguments.of(Boolean.valueOf("No config")) // Have you expected it is a FALSE? or Exception?
                );
    }

    private static Stream<Arguments> filterConfig() {
        Double min = null;
        Double max = null;
        return Stream.of(
                Arguments.of(0.00, 20.00),
                Arguments.of(50.00, 101.00),
                Arguments.of(min, 101.00),
                Arguments.of(0.00, max),
                Arguments.of(min, max)
        );
    }

}