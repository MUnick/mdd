package com.yudenko.mdd.fail;

import com.yudenko.mdd.domain.Payment;

import java.util.Date;

public class PaymentProcessor {

    private final static String TAX_ACCOUNT = "1230000078"; // why similar constants have different name pattern?
    private final static String ACCOUNT_INCOME = "3300000772";
    private final static String ACCOUNT_INCOME_BACK = "5300000772";

    public boolean simplePayment(Payment payment) {
        // calculate tax
        double taxAmount = calculateTax(payment.getAmount(), payment.getTax());
        // calculate amount to transfer
        double amountToTransfer = payment.getAmount() - taxAmount;
        // transfer and return result
        return doTransfer(ACCOUNT_INCOME, amountToTransfer) & doTransfer(TAX_ACCOUNT, taxAmount);
    }

    public boolean simplePayment_2(Payment payment) {
        // calculate tax
        double taxAmount = calculateTax(payment.getAmount(), payment.getTax());
        // transfer amount to main account
        double amountToTransfer = payment.getAmount() - taxAmount;
        boolean mainPaymentReady = doTransfer(ACCOUNT_INCOME, amountToTransfer);
        // transfer tax amount to tax account
        boolean taxPaymentReady = doTransfer(TAX_ACCOUNT, taxAmount);
        return mainPaymentReady && taxPaymentReady;
    }

    public boolean simplePayment_2_FailMiddle(Payment payment) {
        // calculate tax
        double taxAmount = calculateTax(payment.getAmount(), payment.getTax());
        // transfer amount to main account
        double amountToTransfer = payment.getAmount() - taxAmount;
        boolean mainPaymentReady = doTransfer(ACCOUNT_INCOME, amountToTransfer);
        // transfer tax amount to tax account
        boolean taxPaymentReady = true;
        if (payment.getTax() > 0 && payment.getTax() < 100) {
            taxPaymentReady = doTransfer(TAX_ACCOUNT, taxAmount);
        }
        return mainPaymentReady && taxPaymentReady;
    }

    public boolean simplePayment_3(Payment payment) {
        // calculate tax
        double taxAmount = calculateTax(payment.getAmount(), payment.getTax());
        // transfer amount to main account
        double amountToTransfer = payment.getAmount() - taxAmount;
        boolean mainPaymentReady = doTransfer(ACCOUNT_INCOME, amountToTransfer);
        if (mainPaymentReady) {
            // transfer tax amount to tax account
            return doTransfer(TAX_ACCOUNT, taxAmount);
        }
        return false;
    }

    public boolean simplePayment_4(Payment payment) {
        // calculate tax
        double taxAmount = calculateTax(payment.getAmount(), payment.getTax());
        // transfer amount to main account
        double amountToTransfer = payment.getAmount() - taxAmount;
        boolean mainPaymentReady = doTransfer(ACCOUNT_INCOME, amountToTransfer);
        if (mainPaymentReady) {
            // transfer tax amount to tax account
            if (!doTransfer(TAX_ACCOUNT, taxAmount)) {
                // return main money back
                doTransfer(ACCOUNT_INCOME_BACK, amountToTransfer);
                return false;
            }
            return true;
        }
        return false;
    }

    public boolean simplePayment_5(Payment payment) {
        // calculate tax
        double taxAmount = calculateTax(payment.getAmount(), payment.getTax());
        // transfer amount to main account
        double amountToTransfer = payment.getAmount() - taxAmount;
        boolean mainPaymentReady = doTransfer(ACCOUNT_INCOME, amountToTransfer);
        if (mainPaymentReady) {
            // transfer tax amount to tax account
            if (taxAmount > 0 && !doTransfer(TAX_ACCOUNT, taxAmount)) {
                // return main money back
                if (!doTransfer(ACCOUNT_INCOME_BACK, amountToTransfer)) {
                    // TODO we need to mark in some way money has been lost
                }
                return false;
            }
            return true;
        }
        return false;
    }

    public boolean simplePayment_FailFast(Payment payment) {
        // do validation firstly
        if (payment == null) {
            return false;
        }
        if (payment.getAmount() <= 0) {
            return false;
        }
        if (payment.getTax() < 0 || payment.getTax() >= 100) {
            return false;
        }
        if (new Date().before(payment.getDate())) {
            return false;
        }
        // if in allowed limits
        // if is not fraud

        // calculate tax
        double taxAmount = calculateTax(payment.getAmount(), payment.getTax());
        // transfer amount to main account
        double amountToTransfer = payment.getAmount() - taxAmount;
        boolean mainPaymentReady = doTransfer(ACCOUNT_INCOME, amountToTransfer);
        if (mainPaymentReady) {
            // transfer tax amount to tax account
            if (taxAmount > 0 && !doTransfer(TAX_ACCOUNT, taxAmount)) {
                // return main money back
                if (!doTransfer(ACCOUNT_INCOME_BACK, amountToTransfer)) {
                    // TODO we need to mark in some way money has been lost
                }
                return false;
            }
            return true;
        }
        return false;
    }

    private boolean doTransfer(String toAccount, double amount) {
        // here we can have any way to do transfer
        // internal
        // external with api
        // async in some way
        boolean done = true;
        return done;
    }

    private double calculateTax(double amount, double tax) {
        return (amount * tax) / 100;
    }

}
