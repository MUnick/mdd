package com.yudenko.mdd.paranoiac;

import com.yudenko.mdd.domain.Payment;
import com.yudenko.mdd.exception.BusinessException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Date;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

class ParanoiacExample_Realist_Test {
    private ParanoiacExample service;

    @BeforeEach
    public void setUp() {
        service = new ParanoiacExample();
    }

    @Test
    public void shouldCalTax() throws BusinessException {
        // GIVEN
        Payment payment = new Payment(100.00,new Date(),10.00);
        // WHEN
        double actualTax = service.calTax(Optional.of(payment));
        // THEN
        assertEquals(10.00, actualTax);
    }

    @Test()
    public void shouldThrowException() {
        // GIVEN
        Payment payment = null;
        // WHEN
        Exception actualException = Assertions.assertThrows(BusinessException.class, () -> service.calTax(Optional.ofNullable(payment)));
        // THEN
        assertNotNull(actualException);
    }

}