
package com.yudenko.mdd.multithreading;

import com.yudenko.mdd.domain.Payment;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MultiThreading {

    private List<Payment> payments = new ArrayList<>();

    public synchronized void onPayment(double amount, double tax) {
        Payment payment = new Payment(amount, new Date(), tax);
        payments.add(payment);
    }

    public synchronized List<Payment> getPayments() {
        return new ArrayList<>(payments);
        //return payments;
    }

    public synchronized double getAverage() {
        return payments.stream().mapToDouble(Payment::getAmount).average().orElse(0);
    }

    public static void main(String[] args) {
        MultiThreading service = new MultiThreading();
        Runnable producer = () -> {
            while (true) {
                service.onPayment(14.00, 4.00);
            }
        };
        new Thread(producer).start();

        for (int i = 0;;i++) {
            double average = service.getPayments().stream().mapToDouble(Payment::getAmount).average().orElse(0);
            System.out.println(i + ": " + average);
        }
    }
}
