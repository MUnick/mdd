package com.yudenko.mdd.timeouts;

import com.yudenko.mdd.domain.Payment;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class LongDurationOperationTest {

    @ParameterizedTest
    @MethodSource("numberPayments")
    void averageCalc(int numberPayments) {
        // GIVEN
        List<Payment> payments = preparePayments(numberPayments);
        LongDurationOperation service = new LongDurationOperation();
        // WHEN
        double average = service.averageCalc(payments);
        // THEN
        assertTrue(average > 0.00);
    }

    @ParameterizedTest
    @MethodSource("numberPayments")
    void averageCalcDynamic(int numberPayments) {
        // GIVEN
        List<Payment> payments = preparePayments(numberPayments);
        LongDurationOperation service = new LongDurationOperation();
        // WHEN
        double average = service.averageCalcDynamic(payments);
        // THEN
        assertTrue(average > 0.00);
    }

    private List<Payment> preparePayments(int number) {
        Random rnd = new Random();
        List<Payment> payments = new ArrayList<>();
        for (int i = 0; i< number;i++) {
            payments.add(new Payment(rnd.nextDouble(), new Date(), 0.00));
        }
        return payments;
    }

    private static Stream<Arguments> numberPayments() {
        return Stream.of(
                Arguments.of(2),
                Arguments.of(5),
                Arguments.of(9),
                Arguments.of(10),
                Arguments.of(15),
                Arguments.of(20)
        );
    }
}