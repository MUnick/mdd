package com.yudenko.mdd.testCoverage;

import java.util.Random;

import static java.lang.Math.abs;

public class DemoMath {

    public static double multiply(double a, double b) {
        return a * b;
    }

    public static double multiplyRandom(double a, double b) {
        return a * b * new Random().nextInt(2);
    }

    public static double multiplyAbs(double a, double b) {
        return a * abs(b);
    }

}
