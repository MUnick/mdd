package com.yudenko.mdd.testCoverage;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class DemoMathPessimisticTest {

    @ParameterizedTest
    @MethodSource("testDataGenerator")
    void multiply(double a, double b, double expected) {
        // GIVEN
        // WHEN
        double actual = DemoMath.multiply(a,b);
        // THEN
        assertEquals(expected, actual);
    }

    @ParameterizedTest
    @MethodSource("testDataGenerator")
    void multiplyRandom(double a, double b, double expected) {
        // GIVEN
        // WHEN
        double actual = DemoMath.multiplyRandom(a,b);
        // THEN
        assertEquals(expected, actual);
    }

    @ParameterizedTest
    @MethodSource("testDataGenerator")
    void multiplyAbs(double a, double b, double expected) {
        // GIVEN
        // WHEN
        double actual = DemoMath.multiplyAbs(a,b);
        // THEN
        assertEquals(expected, actual);
    }
    private static Stream<Arguments> testDataGenerator() {
        return Stream.of(
                Arguments.of(2, 2, 4),
                Arguments.of(3, 3, 9),
                Arguments.of(2, -2, -4),
                Arguments.of(0.001, 0.001, 0.000001)
        );
    }
}