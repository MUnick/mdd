package com.yudenko.mdd.fail;

import com.yudenko.mdd.domain.Payment;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Date;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class PaymentProcessor_FailFast_Test {
    private PaymentProcessor paymentProcessor;

    @BeforeEach
    void setUp() {
        paymentProcessor = new PaymentProcessor();
    }

    @Test
    void shouldFailForNullPayment() {
        // GIVEN
        Payment payment = null;
        // WHEN
        boolean result = paymentProcessor.simplePayment_FailFast(payment);
        // THEN
        assertFalse(result);
    }

    @ParameterizedTest
    @Disabled
    @MethodSource("invalidPaymentsSource")
    void shouldFailForInvaidPayment(Payment payment) {
        // GIVEN
        // WHEN
        boolean result = paymentProcessor.simplePayment_FailFast(payment);
        // THEN
        assertFalse(result);
    }

    @Test
    void shouldSuccessPayment() {
        // GIVEN
        Payment payment = new Payment(100.00, new Date(), 10.00);
        // WHEN
        boolean result = paymentProcessor.simplePayment(payment);
        // THEN
        assertTrue(result);
        // check 2 accounts contain valid amount
    }

    @Test
    void shouldSuccessPaymentWithoutTax() {
        // GIVEN
        Payment payment = new Payment(100.00, new Date(), 0.00);
        // WHEN
        boolean result = paymentProcessor.simplePayment(payment);
        // THEN
        assertTrue(result);
        // check 2 accounts contain valid amount
        // check was not tax transfer
    }

    @Test
    @Disabled
    void shouldFailIfMainTransferIsFailed() {
        // GIVEN
        // mock transfer for specific fail
        Date yesterday = new Date(new Date().getTime() - 24 * 60 * 60 * 1000);
        Payment payment = new Payment(100.00, yesterday, 9.00);
        // WHEN
        boolean result = paymentProcessor.simplePayment_FailFast(payment);
        // THEN
        assertFalse(result);
        // check 2 accounts contain valid amount - no transfer
    }

    @Test
    @Disabled
    void shouldFailIfTaxTransferIsFailed() {
        // GIVEN
        // mock transfer for specific fail
        Date yesterday = new Date(new Date().getTime() - 24 * 60 * 60 * 1000);
        Payment payment = new Payment(100.00, yesterday, 9.00);
        // WHEN
        boolean result = paymentProcessor.simplePayment_FailFast(payment);
        // THEN
        assertFalse(result);
        // check 2 accounts contain valid amount - no transfer
        // check back account was adjusted with amount to repeat
    }

    private static Stream<Arguments> invalidPaymentsSource() {
        Date yesterday = new Date(new Date().getTime() - 24 * 60 * 60 * 1000);
        Date tomorrow = new Date(new Date().getTime() + 24 * 60 * 60 * 1000);

        return Stream.of(
                Arguments.of(new Payment(1000.00, null, 10.00)), // no date
                Arguments.of(new Payment(1000.00, tomorrow, 10.00)), // no date
                Arguments.of(new Payment(Double.NaN, yesterday, 10.00)),
                Arguments.of(new Payment(Double.POSITIVE_INFINITY, yesterday, 10.00)),
                Arguments.of(new Payment(null, yesterday, 10.00)),
                Arguments.of(new Payment(-500.00, yesterday, 10.00)),
                Arguments.of(new Payment(0.00, yesterday, 10.00)),
                Arguments.of(new Payment(500.00, yesterday, null)),
                Arguments.of(new Payment(500.00, yesterday, -1.00)),
                Arguments.of(new Payment(500.00, yesterday, 100.01))
        );
    }

}