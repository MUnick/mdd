package com.yudenko.mdd.paranoiac;

import com.yudenko.mdd.domain.Payment;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Date;
import java.util.Optional;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class ParanoiacExampleTest {
    private ParanoiacExample service;

    @BeforeEach
    public void setUp() {
        service = new ParanoiacExample();
    }

    @Test
    public void shouldCalcTax() {
        // GIVEN
        Payment payment = new Payment(100.00, new Date(), 10.00);
        // WHEN
        double actualTax = service.calcTaxParanoiacNative(Optional.of(payment));
        // THEN
        assertEquals(10.00, actualTax);
    }

    @ParameterizedTest
    @MethodSource("invalidPaymentsSource")
    public void shouldThrowErrorForInvalidData(Payment payment) {
        // GIVEN
        // WHEN
        Exception actualException = Assertions.assertThrows(IllegalArgumentException.class, () -> service.calcTaxParanoiacNative(Optional.ofNullable(payment)));
        // THEN
        assertNotNull(actualException);
    }

    @Test
    public void shouldThrowErrorForOptionalNull() {
        // GIVEN
        // WHEN
        Exception actualException = Assertions.assertThrows(IllegalArgumentException.class, () -> service.calcTaxParanoiacNative(null));
        // THEN
        assertNotNull(actualException);
    }

    @Test
    public void shouldThrowErrorForOptionalEmpty() {
        // GIVEN
        // WHEN
        Exception actualException = Assertions.assertThrows(IllegalArgumentException.class, () -> service.calcTaxParanoiacNative(Optional.empty()));
        // THEN
        assertNotNull(actualException);
    }

    // We can add more and more tests to be even more paranoid
    // We need to cover 100% of code. I will not stop until code will not covered fully
    private static Stream<Arguments> invalidPaymentsSource() {
        Date yesterday = new Date(new Date().getTime() - 24 * 60 * 60 * 1000);
        Date tomorrow = new Date(new Date().getTime() + 24 * 60 * 60 * 1000);

        return Stream.of(
                Arguments.of(new Payment(1000.00, null, 10.00)), // no date
                Arguments.of(new Payment(1000.00, tomorrow, 10.00)), // no date
                Arguments.of(new Payment(Double.NaN, yesterday, 10.00)),
                Arguments.of(new Payment(Double.POSITIVE_INFINITY, yesterday, 10.00)),
                Arguments.of(new Payment(null, yesterday, 10.00)),
                Arguments.of(new Payment(-500.00, yesterday, 10.00)),
                Arguments.of(new Payment(0.00, yesterday, 10.00)),
                Arguments.of(new Payment(500.00, yesterday, null)),
                Arguments.of(new Payment(500.00, yesterday, -1.00)),
                Arguments.of(new Payment(500.00, yesterday, 100.01))
        );
    }
}