package com.yudenko.mdd.paranoiac;

import com.yudenko.mdd.domain.Payment;
import com.yudenko.mdd.exception.BusinessException;

import java.util.Date;
import java.util.Optional;

public class ParanoiacExample {

    public double calTax(Optional<Payment> payment) throws BusinessException {
        if (!payment.isPresent()) {
            throw new BusinessException();
        }
        return calcInternal(payment.get().getAmount(), payment.get().getTax());
    }

    public double calTaxOptimistic(Optional<Payment> payment) {
        return calcInternal(payment.get().getAmount(), payment.get().getTax());
    }

    public double calcTaxParanoiacNative(Optional<Payment> payment) {
        if (payment == null) {
            throw new IllegalArgumentException();
        }
        if (!payment.isPresent()) {
            throw new IllegalArgumentException();
        }
        Payment paymentValue = payment.orElse(null);
        if (paymentValue == null) {
            throw new IllegalArgumentException();
        }
        Double amount = paymentValue.getAmount();
        if (amount == null) {
            throw new IllegalArgumentException();
        }
        // check if amount is positive
        if (amount.isInfinite() || amount.isNaN()) {
            throw new IllegalArgumentException();
        }
        if (amount <= 0) {
            throw new IllegalArgumentException();
        }
        Double tax = paymentValue.getTax();
        if (tax == null) {
            throw new IllegalArgumentException();
        }
        // check if tax is positive in range
        if (tax < 0 || tax > 100) {
            throw new IllegalArgumentException();
        }
        // check date is exist not future
        Optional<Date> paymentDate = Optional.ofNullable(paymentValue.getDate());
        if (!paymentDate.isPresent()) {
            throw new IllegalArgumentException();
        }
        if (new Date().before(paymentDate.get())) {
            throw new IllegalArgumentException();
        }

        return calcInternal(amount, tax);
    }

    public double calcTaxParanoiacOptional(Optional<Payment> payment) {
        Optional<Optional<Payment>> optPayment = Optional.ofNullable(payment);
        if (!optPayment.isPresent()) {
            throw new IllegalArgumentException();
        }
        if (!optPayment.get().isPresent()) {
            throw new IllegalArgumentException();
        }
        Payment paymentValue = optPayment.get().get();
        Optional<Double> optAmount = Optional.ofNullable(paymentValue.getAmount());
        if (!optAmount.isPresent()) {
            throw new IllegalArgumentException();
        }
        Optional<Double> optTax = Optional.ofNullable(paymentValue.getTax());
        if (!optTax.isPresent()) {
            throw new IllegalArgumentException();
        }

        return calcInternal(optAmount.get(), optTax.get());
    }

    private double calcInternal(double amount, double tax) {
        return (amount * tax) / 100;
    }
}
