package com.yudenko.mdd.heron;

import com.yudenko.mdd.domain.Triangle;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class HeronFormulaTest {

    @Test
    void shouldCalculateAreaOfTriangle() throws Exception {
        //GIVEN
        double a = 3, b = 4, c = 5;
        //WHEN
        double expectedArea = HeronFormula.areaTriangle(a, b, c);
        //THEN
        assertEquals(6, expectedArea);
    }

    @Test
    void shouldHaveException() {
        //GIVEN
        double a = 3, b = 4, c = 10;
        //WHEN
        Exception thrown = assertThrows(Exception.class, () -> HeronFormula.areaTriangle(a, b, c));
        //THEN
        Assertions.assertEquals("wrong parameters", thrown.getMessage());
    }

    @Test
    void shouldHaveExceptionWithOneNegativeParameters() {
        //GIVEN
        double a = -3, b = 4, c = 5;
        //WHEN
        Exception thrown = assertThrows(Exception.class, () -> HeronFormula.areaTriangle(a, b, c));
        //THEN
        Assertions.assertEquals("wrong parameters", thrown.getMessage());
    }

    @Test
    void shouldHaveExceptionWithTwoNegativeParameters() {
        //GIVEN
        double a = -3, b = -4, c = 5;
        //WHEN
        Exception thrown = assertThrows(Exception.class, () -> HeronFormula.areaTriangle(a, b, c));
        //THEN
        Assertions.assertEquals("wrong parameters", thrown.getMessage());
    }

    @Test
    void shouldHaveExceptionWithAllNegativeParameters() {
        //GIVEN
        double a = -3, b = -4, c = -5;
        //WHEN
        Exception thrown = assertThrows(Exception.class, () -> HeronFormula.areaTriangle(a, b, c));
        //THEN
        Assertions.assertEquals("wrong parameters", thrown.getMessage());
    }

    @Test
    void shouldCalculateAreaOfTriangleObject() throws Exception {
        //GIVEN
        double a = 3, b = 4, c = 5;
        Triangle triangle = new Triangle(a, b, c);
        //WHEN
        double expectedArea = HeronFormula.areaTriangleParanoic(triangle);
        //THEN
        assertEquals(6, expectedArea);
    }
}