package com.yudenko.mdd.paranoiac;

import com.yudenko.mdd.domain.Payment;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Date;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

class ParanoiacExample_Optimistic_Test {

    private ParanoiacExample service;

    @BeforeEach
    public void setUp() {
        service = new ParanoiacExample();
    }

    @Test
    public void shouldCalTax() {
        // GIVEN
        Payment payment = new Payment(100.00,new Date(),10.00);
        // WHEN
        double actualTax = service.calTaxOptimistic(Optional.of(payment));
        // THEN
        assertEquals(10.00, actualTax);
    }
}