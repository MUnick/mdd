package com.yudenko.mdd.domain;

public class Triangle {
    final private double a;
    final private double b;
    final private double c;

    public Triangle(double a, double b, double c) throws Exception {
        if ((b + c) > a && (a + c) > b && (a + b) > c) {
            this.a = a;
            this.b = b;
            this.c = c;
        } else {
            throw new Exception("wrong parameters");
        }
    }

    public double getA() {
        return a;
    }

    public double getB() {
        return b;
    }

    public double getC() {
        return c;
    }
}
