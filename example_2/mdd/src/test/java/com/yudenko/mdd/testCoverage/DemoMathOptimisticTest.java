package com.yudenko.mdd.testCoverage;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DemoMathOptimisticTest {

    @Test
    void multiply() {
        // GIVEN
        double a = 2;
        double b = 2;
        // WHEN
        double actual = DemoMath.multiply(a,b);
        // THEN
        assertEquals(4.00, actual);
    }

    @Test
    void multiplyRandom() {
        // GIVEN
        double a = 2;
        double b = 2;
        // WHEN
        double actual = DemoMath.multiplyRandom(a,b);
        // THEN
        assertEquals(4.00, actual);
    }

    @Test
    void multiplyAbs() {
        // GIVEN
        double a = 2;
        double b = 2;
        // WHEN
        double actual = DemoMath.multiplyAbs(a,b);
        // THEN
        assertEquals(4.00, actual);
    }
}